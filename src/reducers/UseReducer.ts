interface stateClassState {
    value: string,
    error: boolean,
    loading: boolean,
    deleted: boolean,
    confirmed: boolean,
}

/**Reducer con condicionales IF*/
const reducerIf = (state: stateClassState, action: { type: string; }) => {
    if (action.type === 'ERROR') {
        return {
            ...state,
            error: true,
            loading: false
        };
    } else if (action.type === 'CHECK') {
        return {
            ...state,
            loading: true
        }
    } else {
        return {
            ...state
        }
    }
}

console.log(reducerIf)



/**Reducer con condicionales SWITCH*/
const reducerSwitch = (state: stateClassState, action: { type: string; }) => {
    switch (action.type) {
        case 'ERROR':
            return {
                ...state,
                error: true,
                loading: false
            };
        case 'CHECK':
            return {
                ...state,
                error: true,
                loading: false
            };

        default:
            return {
                ...state
            };
    }
}

console.log(reducerSwitch)


/**Reducer con condicionales Object*/
const reducerObject = (state: stateClassState) => ({
    'ERROR': {
        ...state,
        error: true,
        loading: false
    },
    'CHECK': {
        ...state,
        error: true,
        loading: false
    }
})

interface reducerState {
    "ERROR": stateClassState,
    "CHECK": stateClassState
}

const reducer = (state: stateClassState, action: { type: keyof reducerState }): stateClassState => {
    const updatedState: reducerState = reducerObject(state);
    return updatedState[action.type] || state;
};

console.log(reducer)