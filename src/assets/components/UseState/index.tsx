import { useState, useEffect } from "react";

interface propsUseState {
    name: string,
}

interface stateClassState {
    value: string,
    error: boolean,
    loading: boolean,
    deleted: boolean,
    confirmed: boolean,
}

const UseState = ({ name }: propsUseState): JSX.Element => {

    const [state, setState] = useState<stateClassState>({
        value: '',
        error: false,
        loading: false,
        deleted: false,
        confirmed: false,
    }
    );

    const SECURITY_CODE = 'paradigma';

    const handleSend = () => setState({ ...state, error: false, loading: false, confirmed: true })
    const handleCheck = (e: React.ChangeEvent<HTMLInputElement>) => setState({ ...state, value: e.target.value });
    const handleLoading = () => setState({ ...state, loading: true })
    const handleError = () => setState({ ...state, error: true, loading: false })
    const handleDelete = () => setState({ ...state, deleted: true })
    const handleReturn = () => setState({ ...state, confirmed: false, value: '' })
    const handleGoBack = () => setState({ ...state, deleted: false, confirmed: false, value: '' })

    useEffect(() => {
        if (state.loading) {
            setTimeout(() => {
                state.value !== SECURITY_CODE ? handleError() : handleSend()
            }, 3000);
        }
    }, [state.loading]);

    return (
        <>
            <h2>{!state.deleted ? `Eliminar ${name}` : `${name} Eliminado`}</h2>
            {
                !state.confirmed && !state.deleted ?
                    <>
                        <p>Por favor, escribe el código de seguridad para comprobar que quieres eliminar</p>
                        {state.error && !state.loading && (
                            <p>Error: El código es incorrecto</p>
                        )}
                        {state.loading && (
                            <p>Cargando...</p>
                        )}
                        <input
                            placeholder="Código de seguridad"
                            value={state.value}
                            onChange={handleCheck} />
                        <button onClick={handleLoading}>Comprobar</button>
                    </>
                    : state.confirmed && !state.deleted ?
                        <>
                            <p>¿Estas seguro de eliminar el UseState?</p>
                            <button onClick={handleDelete}>Si, Eliminar</button>
                            <button onClick={handleReturn}>No, Regresar</button>
                        </>
                        :
                        <>
                            <p>Eliminado con éxito</p>
                            <button onClick={handleGoBack}>Recuperar State</button>
                        </>
            }
        </>
    );

};

export { UseState };