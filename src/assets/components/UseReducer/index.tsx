import { useEffect, useReducer } from "react";

interface propsUseState {
    name: string,
}

interface stateClassState {
    value: string,
    error: boolean,
    loading: boolean,
    deleted: boolean,
    confirmed: boolean,
}


const UseReducer = ({ name }: propsUseState): JSX.Element => {

    const [state, dispatch] = useReducer(reducer, initialState)

    const SECURITY_CODE = 'paradigma';

    const handleSend = () => dispatch({ type: "CONFIRM" })
    const handleCheck = (e: React.ChangeEvent<HTMLInputElement>) => dispatch({ type: "CHANGE", payload: e.target.value });
    const handleLoading = () => dispatch({ type: "LOADING" })
    const handleError = () => dispatch({ type: "ERROR" })
    const handleDelete = () => dispatch({ type: "DELETE" })
    const handleReturn = () => dispatch({ type: "RETURN" })
    const handleGoBack = () => dispatch({ type: "GO_BACK" })

    useEffect(() => {
        if (state.loading) {
            setTimeout(() => {
                state.value !== SECURITY_CODE ? handleError() : handleSend()
            }, 1000);
        }
    }, [state.loading]);

    return (
        <>
            <h2>{!state.deleted ? `Eliminar ${name}` : `${name} Eliminado`}</h2>
            {
                !state.confirmed && !state.deleted ?
                    <>
                        <p>Por favor, escribe el código de seguridad para comprobar que quieres eliminar</p>
                        {state.error && !state.loading && (
                            <p>Error: El código es incorrecto</p>
                        )}
                        {state.loading && (
                            <p>Cargando...</p>
                        )}
                        <input
                            placeholder="Código de seguridad"
                            value={state.value}
                            onChange={handleCheck} />
                        <button onClick={handleLoading}>Comprobar</button>
                    </>
                    : state.confirmed && !state.deleted ?
                        <>
                            <p>¿Estas seguro de eliminar el UseState?</p>
                            <button onClick={handleDelete}>Si, Eliminar</button>
                            <button onClick={handleReturn}>No, Regresar</button>
                        </>
                        :
                        <>
                            <p>Eliminado con éxito</p>
                            <button onClick={handleGoBack}>Recuperar State</button>
                        </>
            }
        </>
    );

};

const initialState: stateClassState = {
    value: '',
    error: false,
    loading: false,
    deleted: false,
    confirmed: false,
}
/**Reducer con condicionales Object*/
const reducerObject = (state: stateClassState, payload: any) => ({
    'ERROR': {
        ...state,
        error: true,
        loading: false
    },
    'CHECK': {
        ...state,
        error: true,
        loading: false
    },
    'CONFIRM': {
        ...state,
        error: false,
        confirmed: true,
        loading: false
    },
    'CHANGE': {
        ...state,
        value: payload
    },
    'DELETE': {
        ...state,
        deleted: true,
    },
    'LOADING': {
        ...state,
        loading: true
    },
    'RETURN': {
        ...state,
        confirmed: false,
        value: ''
    },
    'GO_BACK': {
        ...state,
        deleted: false,
        confirmed: false,
        value: ''
    },
})

interface reducerState {
    "ERROR": stateClassState,
    "CHECK": stateClassState,
    "CONFIRM": stateClassState,
    "CHANGE": stateClassState,
    "DELETE": stateClassState,
    "LOADING": stateClassState,
    "RETURN": stateClassState,
    "GO_BACK": stateClassState,
}

const reducer = (state: stateClassState, action: {
    [x: string]: any; type: keyof reducerState
}): stateClassState => {
    const updatedState: reducerState = reducerObject(state, action.payload);
    return updatedState[action.type] || state;
};

export { UseReducer };