import React from "react";

class Loading extends React.Component {

    //El componente se desmontará
    componentWillUnmount(): void {
        console.log('Se desmontó')
    }

    render(): React.ReactNode {

        return (
            <p>
                <span>Cargando...</span>
            </p>
        )
    }
}

export { Loading };