import React from "react";
import { Loading } from "./Loading";

const SECURITY_CODE = 'paradigma';

interface propsClassState {
    name: string,
}

interface stateClassState {
    value: string,
    error: boolean,
    loading: boolean,
}

class ClassState extends React.Component<propsClassState, stateClassState> {

    constructor(props: propsClassState) {
        super(props);
        this.state = {
            value: '',
            error: false,
            loading: false,
        };
    }

    // //El componente se mostrará
    // UNSAFE_componentWillMount(): void {

    // }

    // //El componente se desmontará
    // componentWillUnmount(): void {

    // }

    // //El componente se mostró
    // componentDidMount(): void {

    // }

    //El componente se actualizó
    componentDidUpdate(): void {
        if (this.state.loading) {
            setTimeout(() => {
                SECURITY_CODE !== this.state.value ? this.setState({ loading: false, error: true }) : this.setState({ loading: false, error: false })
            }, 3000);
        }
    }

    render(): React.ReactNode {

        const { name }: propsClassState = this.props
        const { error, loading, value }: stateClassState = this.state

        return (
            <div>
                <h2>Eliminar {name}</h2>
                <p>Por favor, escribe el código de seguridad para comprobar que quieres eliminar</p>
                {error && !loading && (
                    <p>Error: El código es incorrecto</p>
                )}
                {loading && (
                    <Loading />
                )}
                <input
                    placeholder="Código de seguridad"
                    value={value}
                    onChange={(e) => {
                        this.setState({ value: e.target.value })
                    }}
                />
                <button onClick={() => this.setState({ loading: true })}>Comprobar</button>
            </div>
        )
    }
}

export { ClassState };